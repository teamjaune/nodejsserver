// Load Environement variables
require('dotenv').config();
require('module-alias/register');

const Logger = require('./Utils/Log');

Logger.info('Web Server is Booting');

const { exit } = require('process');

try {
    require('./Database/initMovieDatabase');
    require('./Webserver/initWebServer');
} catch (e) {
    Logger.error(`Failed To Initialize Database or WebServer : ${e}`);
    exit();
}
