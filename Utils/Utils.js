const Logger = require('@Log');

module.exports = {
    escapeSql: function (sql) {
        if (typeof sql !== 'string') {
            Logger.warning('Trying to escape non-string SQL: ' + sql);
            return sql;
        }
        sql = sql.replace(/[àáâãäå]/g, 'a');
        sql = sql.replace(/æ/g, 'ae');
        sql = sql.replace(/ç/g, 'c');
        sql = sql.replace(/[èéêë]/g, 'e');
        sql = sql.replace(/[ìíîï]/g, 'i');
        sql = sql.replace(/ñ/g, 'n');
        sql = sql.replace(/[òóôõö]/g, 'o');
        sql = sql.replace(/œ/g, 'oe');
        sql = sql.replace(/[ùúûü]/g, 'u');
        sql = sql.replace(/[ýÿ]/g, 'y');
        return sql.replace(/'/g, '%').replace(/ /g, '%').replace(/"/g, '%').replace(/`/g, '%');
    },

    replaceSql: function (field) {
        if (typeof field !== 'string') {
            Logger.warning('Trying to replace non-string SQL field: ' + field);
            return field;
        }

        return `TRANSLATE(
                TRANSLATE(
                        REPLACE(
                            TRANSLATE(
                                REPLACE(
                                    TRANSLATE(
                                        TRANSLATE(
                                            REPLACE(
                                                REPLACE(
                                                    TRANSLATE(
                                                        REPLACE(
                                                            REPLACE(
                                                                REPLACE(${field}, ' ', ''),
                                                            '''', ''),
                                                        '"', ''),
                                                    'àáâãäå', 'aaaaaa'),
                                                'æ', 'ae'),
                                            'ç', 'c'),
                                        'èéêë', 'eeee'),
                                    'ìíîï', 'iiii'),
                                'ñ', 'n'),
                            'òóôõö', 'ooooo'),
                        'œ', 'oe'),
                    'ùúûü', 'uuuu'),
                'ýÿ', 'yy')`;
    }
};
