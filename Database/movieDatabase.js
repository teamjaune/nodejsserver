'user strict';
require('dotenv').config(); // include .env file
const { Pool, Client } = require('pg');

class EnhancedClient extends Client {
    getStartupConf() {
        if (process.env.MOVIE_DB_SCHEMA) {
            try {
                const options = JSON.parse(process.env.MOVIE_DB_SCHEMA);
                return {
                    ...super.getStartupConf(),
                    ...options
                };
            } catch (e) {
                console.error(e);
                // Coalesce to super.getStartupConf() on parse error
            }
        }
        return super.getStartupConf();
    }
}

const poolV1 = new Pool({
    Client: EnhancedClient,
    user: process.env.MOVIE_DB_USER,
    host: process.env.MOVIE_DB_HOST,
    database: process.env.MOVIE_DB_NAME,
    password: process.env.MOVIE_DB_PASS,
    port: process.env.MOVIE_DB_PORT
});

module.exports = poolV1;
