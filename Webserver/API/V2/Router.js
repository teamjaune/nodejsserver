// Imports
const ApiAuth = require('./ApiAuthentication');
const Movies = require('./DataProvider/Movies');
const Crew = require('./DataProvider/Crew');
const Companies = require('./DataProvider/Companies');
const Search = require('./DataProvider/Search');

// PATHS
const ENDPOINTS_PATH = '/Api/V2';
const KEY_PROTECTED = `${ENDPOINTS_PATH}/Key`;
const AI = `${KEY_PROTECTED}/Ai`;
module.exports = function (router) {
    // Identity Verification
    router.use(`${KEY_PROTECTED}/*`, ApiAuth.validatekey);

    router.use(`${AI}/*`, ApiAuth.validateAiKey);

    router.route(`${AI}/MovieIds`)
        .get(Movies.getAllMovieIds);

    router.route(`${AI}/MovieDump`)
        .get(Movies.getAllData);

    router.route(`${KEY_PROTECTED}/Movie/:movieID`)
        .get(Movies.getMovieByID);

    router.route(`${KEY_PROTECTED}/Person/:personID`)
        .get(Crew.getPersonByID);

    router.route(`${KEY_PROTECTED}/Company/:companyID`)
        .get(Companies.getCompanyByID);

    router.route(`${KEY_PROTECTED}/Movies`)
        .post(Movies.getMoviesByIDs);

    router.route(`${KEY_PROTECTED}/Search`)
        .post(Search.search);

    router.route(`${KEY_PROTECTED}/Search/Movie`)
        .post(Search.searchMovie);

    router.route(`${KEY_PROTECTED}/Search/People`)
        .post(Search.searchPeople);

    router.route(`${KEY_PROTECTED}/Search/Companies`)
        .post(Search.searchCompanies);
};
