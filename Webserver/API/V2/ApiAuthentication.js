const keys = JSON.parse(process.env.ALLOWED_API_KEYS);
const Aikeys = JSON.parse(process.env.ALLOWED_AI_API_KEYS);

module.exports = {
    validatekey: async function (req, res, next) {
        const key = req.headers.authorization;
        if (!key) {
            res.status(400).json({ error: 'No credentials sent!' });
        } else {
            if (Array.from(keys).indexOf(key) >= 0) {
                if (next !== null) {
                    next();
                }
            } else {
                res.status(403).json({ error: 'Un Authorized' });
            }
        }
    },

    validateAiKey: async function (req, res, next) {
        const key = req.headers.authorization;
        if (!key) {
            res.status(400).json({ error: 'No credentials sent!' });
        } else {
            if (Array.from(Aikeys).indexOf(key) >= 0) {
                if (next !== null) {
                    next();
                }
            } else {
                res.status(403).json({ error: 'Un Authorized' });
            }
        }
    }
};
