const Logger = require('@Log');
const MovieDb = require('@movieDatabase');
const Responses = require('../ErrorResponses');

// Responses
function companyMoviesListReponse(movies) {
    const moviesListOut = [];
    movies.forEach((movie) => {
        const structuredMovie = {};
        structuredMovie.movie_id = movie.movie_id;
        structuredMovie.primary_title = movie.primary_title;
        structuredMovie.title_type = movie.title_type;
        structuredMovie.original_title = movie.original_title;
        structuredMovie.is_adult = movie.is_adult;
        structuredMovie.start_year = movie.start_year;
        structuredMovie.end_year = movie.end_year;
        structuredMovie.runtime_minutes = movie.runtime_minutes;
        structuredMovie.number_votes = movie.number_votes;
        structuredMovie.average_rating = movie.average_rating;
        structuredMovie.image_url = movie.image_url;
        structuredMovie.french_title = movie.french_title;
        moviesListOut.push(structuredMovie);
    });
    return moviesListOut;
}

function companyInfoResponse(company) {
    const companyJson = {};
    companyJson.company_id = company.company_id;
    companyJson.name = company.name;
    companyJson.description = company.description;
    companyJson.image_url = company.image_url;
    companyJson.origin_country = company.origin_country;
    companyJson.headquarters = company.headquarters;
    companyJson.homepage = company.homepage;
    companyJson.movies = companyMoviesListReponse(company.movies);
    return companyJson;
}

// Helper Functions
async function getCompanyMovies(companyID) {
    const sql = `
    SELECT  movies.movie_id
            ,primary_title
            , title_type
            , original_title
            , is_adult
            , start_year
            , end_year
            , runtime_minutes
            , average_rating
            , number_votes
            , movies.image_url
            , french_title 
            FROM companies
            LEFT JOIN production_companies ON production_companies.company_id = companies.company_id
            LEFT JOIN movies ON production_companies.movie_id= movies.movie_id
            WHERE companies.company_id = $1
            ORDER BY movies.number_votes DESC, movies.average_rating DESC, movies.start_year DESC`;

    return new Promise((resolve) => {
        MovieDb.query(sql, [companyID], async function (err, result) {
            if (err) {
                Logger.error(`Error while getting company movies: ${err}`);
                resolve(null);
            }
            resolve(result.rows);
        });
    });
}

async function getCompanyInfo(companyID) {
    const sql = `
    Select company_id
            ,name
            , description
            , image_url
            , origin_country
            , headquarters
            , homepage
        FROM companies
        WHERE company_id = $1 LIMIT 1`;
    return new Promise((resolve, reject) => {
        MovieDb.query(sql, [companyID], async function (err, result) {
            if (err) {
                reject(null);
                Logger.error(`Error while getting company info: ${err}`);
            }
            if (result.rows[0]) {
                result.rows[0].movies = await getCompanyMovies(companyID);
            }
            resolve(result.rows[0]);
        });
    });
}

module.exports = {
    getCompanyByID: async function (req, res) {
        const companyID = req.params.companyID;
        if (!companyID || companyID == null) {
            res.status(400).json(Responses.errorMessage('Company ID must be provided'));
            return;
        }

        getCompanyInfo(companyID).then((fulfilledValue) => {
            if (fulfilledValue) {
                res.status(200).json(companyInfoResponse(fulfilledValue));
            } else {
                res.status(404).json(Responses.errorMessage('Company Not Found'));
            }
        }, (_) => {
            res.status(500).json(Responses.errorMessage('Internal Server Error'));
        });
    }
};
