const Utils = require('@Utils');
const Logger = require('@Log');
const MovieDb = require('@movieDatabase');

const MIN_YEAR_LIMIT = 1850;
const MAX_YEAR_LIMIT = 2050;

const SearchType = {
    MOVIES: 0,
    PEOPLE: 10,
    COMPANIES: 20,
    ALL: 30
};
// ----------------------- RESPONSES -------------------------
function searchCompaniesResponse(companies) {
    const json = [];
    companies.forEach(person => {
        const companiesJson = {};
        companiesJson.company_id = person.company_id;
        companiesJson.name = person.name;
        companiesJson.description = person.description;
        companiesJson.image_url = person.image_url;
        companiesJson.origin_country = person.origin_country;
        json.push(companiesJson);
    });
    return json;
}

function searchPersonResponse(people) {
    const json = [];
    people.forEach(person => {
        const peopleJson = {};
        peopleJson.person_id = person.person_id;
        peopleJson.name = person.name;
        peopleJson.birth_year = person.birth_year;
        peopleJson.death_year = person.death_year;
        peopleJson.image_url = person.image_url;
        peopleJson.biography = person.biography;
        peopleJson.birth_place = person.birth_place;
        peopleJson.number_roles = person.number_roles;
        json.push(peopleJson);
    });
    return json;
}

function searchMovieResponse(movies) {
    const moviesList = [];
    movies.forEach(movie => {
        const movieJson = {};
        movieJson.movie_id = movie.movie_id;
        movieJson.title_type = movie.title_type;
        movieJson.primary_title = movie.primary_title;
        movieJson.original_title = movie.original_title;
        movieJson.is_adult = movie.is_adult;
        movieJson.start_year = movie.start_year;
        movieJson.end_year = movie.end_year;
        movieJson.runtime_minutes = movie.runtime_minutes;
        movieJson.average_rating = movie.average_rating;
        movieJson.number_votes = movie.number_votes;
        movieJson.image_url = movie.image_url;
        movieJson.french_title = movie.french_title;
        movieJson.synopsis = movie.synopsis;
        moviesList.push(movieJson);
    });
    return moviesList;
}
// ----------------------- RESPONSES -------------------------

async function searchMovies(name, genresFilter, familyFriendly, startYear, endYear, offset) {
    if (!name || name === '') {
        return [];
    }

    const genres = genresFilter.join("','");

    const sql = `
        WITH fromMoviesSimpleRequest AS (
            (
                SELECT *
                FROM movies
                WHERE ${Utils.replaceSql('original_title')} ILIKE '${Utils.escapeSql(name)}'
                OR ${Utils.replaceSql('primary_title')} ILIKE '${Utils.escapeSql(name)}'
                OR ${Utils.replaceSql('french_title')} ILIKE '${Utils.escapeSql(name)}'
                ORDER BY number_votes DESC, average_rating DESC, start_year DESC
            )
        ),
        fromMoviesAllRequest AS (
            (
                SELECT * 
                FROM movies
                WHERE (${Utils.replaceSql('original_title')} ILIKE '%${Utils.escapeSql(name)}%'
                OR ${Utils.replaceSql('primary_title')} ILIKE '%${Utils.escapeSql(name)}%'
                OR ${Utils.replaceSql('french_title')} ILIKE '%${Utils.escapeSql(name)}%')
                and NOT EXISTS (select movie_id from fromMoviesSimpleRequest WHERE fromMoviesSimpleRequest.movie_id = movies.movie_id)
                ORDER BY number_votes DESC, average_rating DESC, start_year DESC
            )
        ),
        allMovies AS (
            (
                SELECT *
                FROM fromMoviesSimpleRequest
            )
            UNION ALL
            (
                SELECT *
                FROM fromMoviesAllRequest
            )
        ),
        toExclude AS (
            (
                SELECT movie_id
                FROM genres
                where genre IN ('${genres}')
                
            )
        )

        SELECT allMovies.movie_id
               , title_type
               , primary_title
               , original_title
               , is_adult
               , start_year
               , runtime_minutes
               , average_rating
               , number_votes
               , image_url
               , french_title
               , synopsis
        FROM allMovies left join toExclude on allMovies.movie_id = toExclude.movie_id
        WHERE toExclude.movie_id IS NULL
            ${startYear ? `AND start_year >= '${startYear}'` : ''}
            ${endYear ? `AND (end_year <= '${endYear}' OR end_year IS NULL)` : ''}
            ${familyFriendly ? 'AND allMovies.is_adult = false' : ''}
        LIMIT 10 OFFSET ${offset}
        `;
    return new Promise((resolve) => {
        MovieDb.query(sql, async function (err, result) {
            if (err) {
                Logger.error(`Error while searching movies: ${err}`);
                resolve([]);
            }
            resolve(searchMovieResponse(result.rows));
        });
    });
}

async function searchPeople(name, offset) {
    if (name == null || name === '') {
        return [];
    }
    const sql = `
    WITH cte AS (SELECT crew.person_id AS person_id, COUNT(crew.person_id) AS cpt
    FROM person INNER JOIN crew ON person.person_id = crew.person_id
    WHERE ${Utils.replaceSql('name')} ILIKE '%${Utils.escapeSql(name)}%'
    GROUP BY crew.person_id
    ORDER BY cpt DESC NULLS LAST OFFSET $1 limit 10)

    SELECT person.person_id, name, birth_year, death_year, image_url, biography, birth_place, cte.cpt as number_roles
    FROM person INNER JOIN cte ON person.person_id = cte.person_id
    ORDER BY cte.cpt DESC NULLS LAST
    LIMIT 10`;
    return new Promise((resolve) => {
        MovieDb.query(sql, [offset], async function (err, result) {
            if (err) {
                Logger.error(`Error while searching people: ${err}`);
                resolve([]);
            }
            resolve(searchPersonResponse(result.rows));
        });
    });
}

async function searchCompanies(name, offset) {
    if (name == null || name === '') {
        return [];
    }
    const sql = `
    WITH cte AS (SELECT production_companies.company_id AS company_id, COUNT(production_companies.company_id) AS cpt
    FROM companies INNER JOIN production_companies ON companies.company_id = production_companies.company_id
    WHERE ${Utils.replaceSql('name')} ILIKE '%${Utils.escapeSql(name)}%'
    GROUP BY production_companies.company_id
    ORDER BY cpt DESC NULLS LAST OFFSET $1 limit 10)

    SELECT companies.company_id, companies.name, companies.description, companies.origin_country, companies.image_url, cte.cpt as number_movies
    FROM companies INNER JOIN cte ON companies.company_id = cte.company_id
    ORDER BY cte.cpt DESC NULLS LAST
    LIMIT 10`;
    return new Promise((resolve) => {
        MovieDb.query(sql, [offset], async function (err, result) {
            if (err) {
                Logger.error(`Error while searching companies: ${err}`);
                resolve([]);
            }
            resolve(searchCompaniesResponse(result.rows));
        });
    });
}

async function searchWithType(body, type) {
    const genres = body.genresToIgnore ? body.genresToIgnore : [];
    const familyFriendly = body.familyFriendly ? body.familyFriendly : false;
    const startYear = Number(body.startYear) ? body.startYear : MIN_YEAR_LIMIT;
    const endYear = Number(body.endYear) ? body.endYear : MAX_YEAR_LIMIT;
    const offset = Number(body.offset) ? body.offset : 0;
    const respBody = {};
    if (type === SearchType.MOVIES || type === SearchType.ALL) {
        respBody.movies = await searchMovies(body.searchText, genres, familyFriendly, startYear, endYear, offset);
    }
    if (type === SearchType.PEOPLE || type === SearchType.ALL) {
        respBody.people = await searchPeople(body.searchText, offset);
    }
    if (type === SearchType.COMPANIES || type === SearchType.ALL) {
        respBody.companies = await searchCompanies(body.searchText, offset);
    }
    return respBody;
}

module.exports = {
    searchMovie: async function (req, res) {
        const body = req.body;
        const result = await searchWithType(body, SearchType.MOVIES);
        res.status(200).json(result.movies);
    },

    search: async function (req, res) {
        const body = req.body;
        const result = await searchWithType(body, SearchType.ALL);
        res.status(200).json(result);
    },

    searchPeople: async function (req, res) {
        const body = req.body;
        const result = await searchWithType(body, SearchType.PEOPLE);
        res.status(200).json(result.people);
    },

    searchCompanies: async function (req, res) {
        const body = req.body;
        const result = await searchWithType(body, SearchType.COMPANIES);
        res.status(200).json(result.companies);
    }
};
