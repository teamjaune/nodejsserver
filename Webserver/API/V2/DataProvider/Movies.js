const Logger = require('@Log');
const MovieDb = require('@movieDatabase');
const ErrorResponses = require('../ErrorResponses');

// Responses
function movieGetInfoResponse(movie) {
    const movieJson = {};
    if (!movie) {
        return movieJson;
    }
    movieJson.movie_id = movie.movie_id;
    movieJson.title_type = movie.title_type;
    movieJson.primary_title = movie.primary_title;
    movieJson.original_title = movie.original_title;
    movieJson.is_adult = movie.is_adult;
    movieJson.start_year = movie.start_year;
    movieJson.end_year = movie.end_year;
    movieJson.runtime_minutes = movie.runtime_minutes;
    movieJson.average_rating = movie.average_rating;
    movieJson.number_votes = movie.number_votes;
    movieJson.image_url = movie.image_url;
    movieJson.french_title = movie.french_title;
    movieJson.synopsis = movie.synopsis;
    movieJson.crew = movie.crew;
    movieJson.genres = movie.genres;
    movieJson.keywords = movie.keywords;
    movieJson.production_companies = movie.production_companies;
    movieJson.watch_methods = movie.watch_methods;
    movieJson.watch_methods_languages = movie.watch_methods_languages;
    return movieJson;
}

function movieCrewInfoResponse(person) {
    const personJson = {};
    if (!person) {
        return personJson;
    }
    personJson.person_id = person.person_id;
    personJson.category = person.category;
    personJson.name = person.name;
    personJson.birth_year = person.birth_year;
    personJson.death_year = person.death_year;
    personJson.image_url = person.image_url;
    return personJson;
}

// Helper Functions
async function getMoviesByIDs(movieIDs) {
    const sql = `
            SELECT movie_id
                , title_type
                , primary_title
                , original_title
                , is_adult
                , start_year
                , end_year
                , runtime_minutes
                , average_rating
                , number_votes
                , image_url
                , french_title
                , synopsis
            FROM movies
            WHERE movies.movie_id IN ('${movieIDs.join("','")}')`;

    return new Promise((resolve, reject) => {
        MovieDb.query(sql, async function (err, result) {
            if (err) {
                Logger.error(`Error while getting movies info: ${err}`);
                reject(null);
            }
            if (result.rows) {
                resolve(result.rows);
            }
            reject([]);
        });
    });
}

async function getMovieByID(movieID) {
    const sql = `
            SELECT movie_id
                , title_type
                , primary_title
                , original_title
                , is_adult
                , start_year
                , end_year
                , runtime_minutes
                , average_rating
                , number_votes
                , image_url
                , french_title
                , synopsis
            FROM movies
            WHERE movies.movie_id = $1
            LIMIT 1`;

    return new Promise((resolve, reject) => {
        MovieDb.query(sql, [movieID], async function (err, result) {
            if (err) {
                Logger.error(`Error while getting movie info: ${err}`);
                reject(null);
            }
            if (result.rows[0]) {
                const dbres = result.rows[0];
                dbres.genres = await getMovieGenres(movieID);
                dbres.keywords = await getMovieKeywords(movieID);
                dbres.crew = await getMovieCrewInfo(movieID);
                dbres.production_companies = await getProductionCompanies(movieID);
                dbres.watch_methods = await getMovieWatchMethods(movieID);
                dbres.watch_methods_languages = await getMovieWatchMethodsLanguages(movieID);
                resolve(dbres);
            }
            reject([]);
        });
    });
}

async function getMovieIds() {
    const sql = `
            SELECT movie_id
            FROM movies`;

    return new Promise((resolve, reject) => {
        MovieDb.query(sql, async function (err, result) {
            if (err) {
                Logger.error(`Error while getting all movie ids info: ${err}`);
                reject(null);
            }
            if (result.rows) {
                resolve(result.rows);
            }
            reject([]);
        });
    });
}

async function getMovieGenres(movieID) {
    const sql = `
        SELECT genre 
        FROM genres 
        WHERE
        movie_id = $1`;
    return new Promise((resolve) => {
        MovieDb.query(sql, [movieID], function (err, result) {
            if (err) {
                resolve([]);
                Logger.error(`Error while movie genres: ${err}`);
            }
            if (result.rows) {
                const flatMap = result.rows.map(x => x.genre);
                resolve(flatMap);
            }
            resolve([]);
        });
    });
}

async function getMovieWatchMethodsLanguages(movieID) {
    const sql = `
        SELECT DISTINCT country  
        FROM how_to_watch
        WHERE movie_id = $1`;
    return new Promise((resolve) => {
        MovieDb.query(sql, [movieID], function (err, result) {
            if (err) {
                resolve([]);
                Logger.error(`Error while getting movie watch methods languages: ${err}`);
            }
            resolve(result.rows);
        });
    });
}

async function getMovieWatchMethods(movieID) {
    const sql = `
        SELECT how_to_watch.provider_id, name, country, type, image_url, display_priority 
        FROM how_to_watch
        INNER JOIN providers ON providers.provider_id = how_to_watch.provider_id
        WHERE movie_id = $1
        ORDER BY display_priority ASC`;
    return new Promise((resolve) => {
        MovieDb.query(sql, [movieID], function (err, result) {
            if (err) {
                resolve([]);
                Logger.error(`Error while movie watch methods: ${err}`);
            }
            resolve(result.rows);
        });
    });
}

async function getAllDataForAI() {
    const sqlMovieInformations = 'SELECT movie_id FROM movies';
    return new Promise((resolve) => {
        MovieDb.query(sqlMovieInformations,
            (error, results) => {
                if (error) {
                    throw error;
                }

                const myMap = new Map();

                for (const movie of results.rows) {
                    myMap.set(movie.movie_id, movie);
                }

                const sqlGenresInformations = 'SELECT movie_id, genre FROM genres';
                MovieDb.query(sqlGenresInformations,
                    (error, result) => {
                        if (error) {
                            throw error;
                        }

                        for (const genres of result.rows) {
                            const movie = myMap.get(genres.movie_id);
                            if (movie.genres !== undefined) {
                                movie.genres += ' ' + genres.genre;
                            } else {
                                movie.genres = genres.genre;
                            }
                            myMap.set(genres.movie_id, movie);
                        }

                        const sqlGetDirectors = `
                        SELECT movie_id
                                , name
                                , category
                        FROM crew 
                        INNER JOIN person ON crew.person_id = person.person_id 
                        WHERE (category = 'director' OR category = 'actor' OR category = 'actress')`;
                        MovieDb.query(sqlGetDirectors,
                            async (error, resCrew) => {
                                if (error) {
                                    throw error;
                                }

                                for (const director of resCrew.rows) {
                                    const movie = myMap.get(director.movie_id);
                                    if (director.category === 'director') {
                                        movie.director = director.name;
                                    } else if (director.category === 'actor' || director.category === 'actress') {
                                        if (movie.cast !== undefined) {
                                            movie.cast += ' ' + director.name;
                                        } else {
                                            movie.cast = director.name;
                                        }
                                    }
                                    myMap.set(director.movie_id, movie);
                                }

                                const result = [];
                                myMap.forEach(function (value, key) {
                                    result.push(value);
                                });

                                const sqlGetCompanies = `
                                                SELECT movie_id, name 
                                                FROM production_companies 
                                                INNER JOIN companies ON production_companies.company_id = companies.company_id`;
                                MovieDb.query(sqlGetCompanies,
                                    async (error, resCompanies) => {
                                        if (error) {
                                            throw error;
                                        }

                                        for (const company of resCompanies.rows) {
                                            const movie = myMap.get(company.movie_id);
                                            if (movie.companies !== undefined) {
                                                movie.companies += ' ' + company.name;
                                            } else {
                                                movie.companies = company.name;
                                            }
                                            myMap.set(company.movie_id, movie);
                                        }

                                        const result = [];
                                        myMap.forEach(function (value, key) {
                                            result.push(value);
                                        });

                                        const sqlGetKeywords = 'SELECT movie_id, keyword FROM keywords';
                                        MovieDb.query(sqlGetKeywords,
                                            async (error, resKeywords) => {
                                                if (error) {
                                                    throw error;
                                                }

                                                for (const keyword of resKeywords.rows) {
                                                    const movie = myMap.get(keyword.movie_id);
                                                    if (movie.keywords !== undefined) {
                                                        movie.keywords += ' ' + keyword.keyword;
                                                    } else {
                                                        movie.keywords = keyword.keyword;
                                                    }
                                                    myMap.set(keyword.movie_id, movie);
                                                }

                                                const result = [];
                                                myMap.forEach(function (value, key) {
                                                    result.push(value);
                                                });

                                                resolve(result);
                                            });
                                    });
                            });
                    });
            });
    });
}

async function getMovieCrewInfo(movieID) {
    const sql = `
    SELECT person.person_id
        ,category
        , name
        , birth_year
        , death_year
        , image_url
    FROM crew 
    LEFT JOIN person ON person.person_id = crew.person_id
    WHERE movie_id = $1
    `;
    return new Promise((resolve) => {
        MovieDb.query(sql, [movieID], async function (err, result) {
            if (err) {
                resolve(null);
                Logger.error(`Error while getting movie crew: ${err}`);
            }
            if (result.rows) {
                const flatMap = result.rows.map(person => movieCrewInfoResponse(person));
                resolve(flatMap);
            }
            resolve(null);
        });
    });
}

async function getMovieKeywords(movieID) {
    const sql = `
        SELECT keyword 
        FROM keywords
        WHERE movie_id = $1`;
    return new Promise((resolve) => {
        MovieDb.query(sql, [movieID], function (err, result) {
            if (err) {
                resolve([]);
                Logger.error(`Error while getting keywords: ${err}`);
            }
            if (result.rows) {
                const flatMap = result.rows.map(x => x.keyword);
                resolve(flatMap);
            }
            resolve([]);
        });
    });
}

async function getProductionCompanies(movieID) {
    const sql = `
        SELECT companies.company_id, companies.name 
        FROM companies 
        INNER JOIN production_companies ON companies.company_id = production_companies.company_id 
        INNER JOIN movies ON production_companies.movie_id = movies.movie_id 
        WHERE movies.movie_id = $1`;
    return new Promise((resolve) => {
        MovieDb.query(sql, [movieID], function (err, result) {
            if (err) {
                resolve([]);
                Logger.error(`Error while getting production companies: ${err}`);
            }
            resolve(result.rows);
        });
    });
}

module.exports = {
    getAllData: async function (req, res) {
        getAllDataForAI().then((onFulfilledResult) => {
            res.status(200).json(onFulfilledResult);
        }, (onRejectResult) => {
            if (onRejectResult == null) {
                res.status(500).json(ErrorResponses.errorMessage('Internal Server Error'));
            }
            res.status(404).json(ErrorResponses.errorMessage('All data Not Found'));
        });
    },

    getAllMovieIds: async function (req, res) {
        getMovieIds().then((onFulfilledResult) => {
            res.status(200).json(onFulfilledResult);
        }, (onRejectResult) => {
            if (onRejectResult == null) {
                res.status(500).json(ErrorResponses.errorMessage('Internal Server Error'));
            }
            res.status(404).json(ErrorResponses.errorMessage('Movie Ids Not Found'));
        });
    },

    getMovieByID: async function (req, res) {
        const movieID = req.params.movieID;
        if (!movieID || movieID == null) {
            res.status(400).json(ErrorResponses.errorMessage('Movie ID must be provided'));
            return;
        }
        getMovieByID(movieID).then((onFulfilledResult) => {
            res.status(200).json(movieGetInfoResponse(onFulfilledResult));
        }, (onRejectResult) => {
            if (onRejectResult == null) {
                res.status(500).json(ErrorResponses.errorMessage('Internal Server Error'));
            }
            res.status(404).json(ErrorResponses.errorMessage('Movie Not Found'));
        });
    },

    getMoviesByIDs: async function (req, res) {
        const movieIDs = req.body.movieIDs;
        if (!movieIDs || movieIDs == null || movieIDs.length === 0) {
            res.status(400).json(ErrorResponses.errorMessage('Movie IDs must be provided'));
            return;
        }
        getMoviesByIDs(movieIDs).then((onFulfilledResult) => {
            const result = [];
            for (const movie of onFulfilledResult) {
                result.push(movieGetInfoResponse(movie));
            }
            res.status(200).json(result);
        }, (onRejectResult) => {
            if (onRejectResult == null) {
                res.status(500).json(ErrorResponses.errorMessage('Internal Server Error'));
            }
            res.status(404).json(ErrorResponses.errorMessage('Movies Not Found'));
        });
    }
};
