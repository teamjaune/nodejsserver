const Logger = require('@Log');
const MovieDb = require('@movieDatabase');
const Responses = require('../ErrorResponses');

// Responses
function personMoviesListReponse(movies) {
    const moviesListOut = [];
    movies.forEach((movie) => {
        const structuredMovie = {};
        structuredMovie.movie_id = movie.movie_id;
        structuredMovie.primary_title = movie.primary_title;
        structuredMovie.title_type = movie.title_type;
        structuredMovie.original_title = movie.original_title;
        structuredMovie.is_adult = movie.is_adult;
        structuredMovie.start_year = movie.start_year;
        structuredMovie.end_year = movie.end_year;
        structuredMovie.runtime_minutes = movie.runtime_minutes;
        structuredMovie.number_votes = movie.number_votes;
        structuredMovie.average_rating = movie.average_rating;
        structuredMovie.image_url = movie.image_url;
        structuredMovie.french_title = movie.french_title;
        structuredMovie.role_category = movie.role_category;
        moviesListOut.push(structuredMovie);
    });
    return moviesListOut;
}

function personInfoResponse(person) {
    const personJson = {};
    personJson.person_id = person.person_id;
    personJson.name = person.name;
    personJson.birth_year = person.birth_year;
    personJson.death_year = person.death_year;
    personJson.image_url = person.image_url;
    personJson.biography = person.biography;
    personJson.birth_place = person.birth_place;
    personJson.movies = personMoviesListReponse(person.movies);
    return personJson;
}

// Helper Functions
async function getPersonMovies(personID) {
    const sql = `
    SELECT  movies.movie_id
            ,primary_title
            , title_type
            , original_title
            , is_adult
            , start_year
            , end_year
            , runtime_minutes
            , average_rating
            , number_votes
            , movies.image_url
            , french_title
            , category
            FROM crew
            LEFT JOIN person ON person.person_id = crew.person_id
            LEFT JOIN movies ON movies.movie_id = crew.movie_id
            WHERE person.person_id = $1`;

    return new Promise((resolve, reject) => {
        MovieDb.query(sql, [personID], async function (err, result) {
            if (err) {
                Logger.error(`Error while getting person movies: ${err}`);
                resolve(null);
            }
            const categories = result.rows.reduce((r, a) => {
                r[a.movie_id] = [...r[a.movie_id] || [], a.category];
                return r;
            }, {});
            result.rows = result.rows.filter((a, i) => result.rows.findIndex((s) => a.movie_id === s.movie_id) === i);
            result.rows.forEach(element => {
                element.role_category = categories[element.movie_id];
            });
            resolve(result.rows);
        });
    });
}

async function getPersonInfo(personID) {
    const sql = `
    Select person_id
            ,name
            , birth_year
            , death_year
            , image_url
            , biography
            , birth_place
        FROM person
        WHERE person_id = $1 LIMIT 1`;
    return new Promise((resolve, reject) => {
        MovieDb.query(sql, [personID], async function (err, result) {
            if (err) {
                reject(null);
                Logger.error(`Error while getting person info: ${err}`);
            }
            if (result.rows[0]) {
                result.rows[0].movies = await getPersonMovies(personID);
            }
            resolve(result.rows[0]);
        });
    });
}

module.exports = {
    getPersonByID: async function (req, res) {
        const personID = req.params.personID;
        if (!personID || personID == null) {
            res.status(400).json(Responses.errorMessage('Person ID must be provided'));
            return;
        }

        getPersonInfo(personID).then((fulfilledValue) => {
            if (fulfilledValue) {
                res.status(200).json(personInfoResponse(fulfilledValue));
            } else {
                res.status(404).json(Responses.errorMessage('Person Not Found'));
            }
        }, (_) => {
            res.status(500).json(Responses.errorMessage('Internal Server Error'));
        });
    }
};
