// Web Server
const Http = require('http');
const Express = require('express');
const CookieParser = require('cookie-parser');
const App = Express();
const Port = 8180;
const Logger = require('@Log');
const ResponseTime = require('response-time');

App.use(ResponseTime());

App.use(CookieParser());
App.use(Express.json()); // To parse the incoming requests with JSON payloads
App.use(Express.static('Webserver/public'));

// V2
const v2Router = require('./API/V2/Router');
v2Router(App);

Http.createServer(App).listen(Port);
Logger.success(`Web Server is Up On Port : ${Port}`);
